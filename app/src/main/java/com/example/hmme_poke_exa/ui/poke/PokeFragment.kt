package com.example.hmme_poke_exa.ui.poke

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.hmme_poke_exa.R
import com.example.hmme_poke_exa.adapter.PokeAdapter
import com.example.hmme_poke_exa.databinding.PokeFragmentBinding
import com.example.hmme_poke_exa.model.PokeListLocation
import com.example.hmme_poke_exa.network.RetrofitService
import com.example.hmme_poke_exa.repository.PokeRepository
import com.example.hmme_poke_exa.ui.locationpoke.LocationFragment

class PokeFragment : Fragment() {
    private lateinit var binding: PokeFragmentBinding

    lateinit var viewModel: PokeViewModel
    private val retrofitService = RetrofitService.getInstance()
    val adapter = PokeAdapter()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = PokeFragmentBinding.inflate(inflater, container,false)
        viewModel = ViewModelProvider(this, PokeViewModelFactory(PokeRepository(retrofitService))).get(PokeViewModel::class.java)

        viewModel.getAllPokemon()

        binding.recyclerview.adapter = adapter

        viewModel.pokeList.observe(viewLifecycleOwner, {
            if(it != null){
                binding.progress.visibility = View.GONE
                adapter.setPokeList(it.results)
            }
        })

        adapter.setOnItemClickListener(object : PokeAdapter.onItemClickListener {
            override fun onItemClick(position: Int) {
                var pokemonSeleccionado = adapter.getObject(position)
                showFragment(pokemonSeleccionado)
            }
        })
        return binding.root
    }

    fun showFragment(pokemonSeleccionado: PokeListLocation){
        val bundle = Bundle()
        val fragment = LocationFragment()
        bundle.putParcelable("model", pokemonSeleccionado)
        fragment.setArguments(bundle)
        val fram = activity?.supportFragmentManager?.beginTransaction()
        fram?.replace(R.id.fragment_main,fragment)
        fram?.commit()
    }
}

