package com.example.hmme_poke_exa.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.hmme_poke_exa.databinding.AdapterPokeBinding
import com.example.hmme_poke_exa.model.Location
import com.example.hmme_poke_exa.model.PokeList
import com.example.hmme_poke_exa.model.PokeListLocation

class PokeAdapter : RecyclerView.Adapter<PokeViewHolder>() {

    var pokemon = mutableListOf<PokeListLocation>()
    private lateinit var mLister: onItemClickListener

    interface onItemClickListener{
        fun onItemClick(position: Int)
    }

    fun setOnItemClickListener(listener: onItemClickListener){
        mLister = listener
    }

    fun setPokeList(pokemon: List<PokeList>){
        val list_poke: MutableList<PokeListLocation> = ArrayList()
        for(item in pokemon){
            val pokeList = PokeListLocation(item.name, item.url, getLocation())
            list_poke.add(pokeList)
        }
        if(list_poke !=null){
            this.pokemon = list_poke
        }
        notifyDataSetChanged()
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PokeViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val bindign = AdapterPokeBinding.inflate(inflater,parent,false)
        return PokeViewHolder(bindign,mLister)
    }

    override fun onBindViewHolder(holder: PokeViewHolder, position: Int) {
        val poke = pokemon.get(position)
        holder.binding.idName.text = poke.namepoke
    }

    override fun getItemCount(): Int {
      return pokemon.size
    }

     fun getObject(int: Int):PokeListLocation{
        return pokemon.get(int);
    }

    fun getLocation(): Location{
        //var longitude = Math.random() *(85*2)-85
        //var latitude = Math.random()*(180*2)-180
        val nLongitud = mutableListOf(-100.315258, -99.1331785 , -99.25346070738807, 1.8883335, -4.8379791)
        val nLatitud= mutableListOf(25.6802019, 19.4326296, 19.385984777422323, 46.603354, 39.3260685)
        var longitude = nLongitud.random()
        var latitude = nLatitud.random()
        val ubi = Location(longitude,latitude)
        return ubi
    }
}

class PokeViewHolder (val binding: AdapterPokeBinding, listener: PokeAdapter.onItemClickListener) : RecyclerView.ViewHolder(binding.root) {

     init {
         binding.cardviewitem.setOnClickListener {
             listener.onItemClick(adapterPosition)
         }
      }

}
