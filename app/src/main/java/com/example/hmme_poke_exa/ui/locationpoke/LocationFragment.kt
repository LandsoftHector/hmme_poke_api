package com.example.hmme_poke_exa.ui.locationpoke

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.hmme_poke_exa.R
import com.example.hmme_poke_exa.model.PokeListLocation
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions

private const val ARG_PARAM1 = "model"

class LocationFragment : Fragment(), OnMapReadyCallback {

    private lateinit var map: GoogleMap
    private  var param1: PokeListLocation? = null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.location_fragment,container,false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments.let {
            if (it != null) {
                param1 = it.getParcelable(ARG_PARAM1)
            }
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        createFragment()
    }

    private fun createFragment() {
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }


    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        createMarker()
    }

    private fun createMarker() {
        if(param1?.location!=null){
            val favoritePlace = LatLng(param1?.location?.latitud!!,param1?.location?.longitud!!)
            map.addMarker(MarkerOptions().position(favoritePlace).title(param1?.namepoke +"-" + param1?.url).visible(true))
            map.animateCamera(
                CameraUpdateFactory.newLatLngZoom(favoritePlace, 8f),
                4000,
                null
            )
        }
    }
}