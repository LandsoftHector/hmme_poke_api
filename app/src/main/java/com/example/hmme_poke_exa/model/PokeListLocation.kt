package com.example.hmme_poke_exa.model

import android.os.Parcel
import android.os.Parcelable

class PokeListLocation() :  Parcelable {

     lateinit var namepoke: String
     lateinit var url : String
     lateinit var location : Location

    constructor(parcel: Parcel) : this() {
        namepoke = parcel.readString().toString()
        url = parcel.readString().toString()
    }


    constructor(namepoke: String,url : String, location : Location) : this() {
        this.namepoke = namepoke
        this.url = url
        this.location = location
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(namepoke)
        parcel.writeString(url)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<PokeListLocation> {
        override fun createFromParcel(parcel: Parcel): PokeListLocation {
            return PokeListLocation(parcel)
        }

        override fun newArray(size: Int): Array<PokeListLocation?> {
            return arrayOfNulls(size)
        }
    }

}

class Location {
    var longitud : Double
    var latitud : Double

    constructor(longitud : Double, latitud : Double){
        this.longitud = longitud
        this.latitud = latitud
    }
}