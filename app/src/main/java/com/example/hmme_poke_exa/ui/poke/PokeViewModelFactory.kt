package com.example.hmme_poke_exa.ui.poke

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.hmme_poke_exa.repository.PokeRepository

class PokeViewModelFactory constructor(private val repository: PokeRepository): ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return if (modelClass.isAssignableFrom(PokeViewModel::class.java)) {
            PokeViewModel(this.repository) as T
        } else {
            throw IllegalArgumentException("ViewModel Not Found")
        }
    }
}