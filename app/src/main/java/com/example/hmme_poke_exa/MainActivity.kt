package com.example.hmme_poke_exa

import android.location.Location
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.activity.result.ActivityResultCaller
import com.example.hmme_poke_exa.databinding.ActivityMainBinding
import com.example.hmme_poke_exa.ui.poke.PokeFragment
import kotlin.math.cos
import kotlin.math.sin
import kotlin.math.sqrt
import kotlin.random.Random

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnShow.setOnClickListener {
            binding.imgPoke.visibility = View.GONE
            binding.fragmentMain.visibility = View.VISIBLE
            val fragment = PokeFragment()
            showFragment(fragment)
        }
    }
    fun showFragment(fragment: PokeFragment){
        val fram = supportFragmentManager.beginTransaction()
        fram.replace(R.id.fragment_main,fragment)
        fram.commit()
    }
}