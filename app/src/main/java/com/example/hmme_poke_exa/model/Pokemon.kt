package com.example.hmme_poke_exa.model

data class Pokemon (val count: Int, val next: String, val previous: String, val results: List<PokeList>) {
}