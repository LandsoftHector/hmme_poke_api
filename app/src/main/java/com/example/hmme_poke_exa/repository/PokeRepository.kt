package com.example.hmme_poke_exa.repository

import com.example.hmme_poke_exa.network.RetrofitService

class PokeRepository constructor(private val retrofitService: RetrofitService) {

    fun getAllMovies() = retrofitService.getAllMovies()
}