package com.example.hmme_poke_exa.ui.poke

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.hmme_poke_exa.model.Pokemon
import com.example.hmme_poke_exa.repository.PokeRepository
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PokeViewModel constructor(private val repository: PokeRepository)  : ViewModel() {

    val pokeList = MutableLiveData<Pokemon>()
    val errorMessage = MutableLiveData<String>()

    fun getAllPokemon() {

        val response = repository.getAllMovies()
        response.enqueue(object : Callback<Pokemon> {
            override fun onResponse(call: Call<Pokemon>, response: Response<Pokemon>) {
                if(response.isSuccessful){
                    pokeList.postValue(response.body())
                }
            }

            override fun onFailure(call: Call<Pokemon>, t: Throwable) {
                errorMessage.postValue(t.message)
            }
        })

    }
}